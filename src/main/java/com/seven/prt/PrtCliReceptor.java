package com.seven.prt;

import sibs.deswin.lib.com.IPersComConsumer;
import sibs.deswin.lib.com.PersConnection;
import sibs.deswin.lib.com.PersServerConnection;
import sibs.deswin.lib.com.SocketIO;
import sibs.deswin.lib.io.ByteBuffer;
import sibs.deswin.lib.util.LogStream;

/**
 * 
 * Simulates a prt server and sends a mocked message as soon as a prt client connects.
 * Very useful for integration tests with the following seven projects:
 * 
 * - EzSuiteInterfaceExternal (Socket-Epms variant)
 * 
 * 
 * PS: It's important to remove any AppName or session configuration on the client. 
 * 
 */
public class PrtCliReceptor implements IPersComConsumer {
	private final int HEADER_LENGHT = 6;
	
	private String _epmsMsg = "";
	
	private long request_mmt_reference = 0;
	
	//private int _nseq = 0; // removido na implementacao de MZ
	
	private int sequenceNo = 0;

	private int _tpMsg = 0x01;

	private int _tmOut = 0x02;

	//private int _mxPed = 1;
	
	public String response_code = "";
	public String response_msg = "";

	@Override
	public void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}
	
	public void setSequence(int nseq) {
		sequenceNo = nseq;
	}

	
	public long getRequest_mmt_reference() {
		return request_mmt_reference;
	}

	public void setRequest_mmt_reference(long request_reference) {
		this.request_mmt_reference = request_reference;
	}

	public void setEpmsMsg(String epmsMsg)
	{	
		_epmsMsg = epmsMsg;
	}
	
	public void setTpMsg(int tpMsg) {
		_tpMsg = tpMsg;
	}

	/*
	public void setMxPed(int mxPed) {
		_mxPed = mxPed;
	}
*/
	public void setTimeOut(int tmOut) {
		_tmOut = tmOut;
	}

	public void dspMessage(SocketIO sio, ByteBuffer bb) {
		try {
			int ofs = bb.getOffset();

			System.out.println("Receiving message (" + request_mmt_reference + ") on PRT"); // ... Offset info : " + ofs);
			
			byte[] buf = bb.getBuffer();
			
			byte rspType = buf[ofs+3]; // response type: 1(timeout), 2(no prt window), 3 (session closed), 4(transporte inativo)
//			Logger.Debug("Receiving message: " + buf);
			
			if (rspType == 0x05) { // Ack a mensagem de pedido		
				response_code = ""+rspType;
				return;
			} 
			else
			{				
				if (rspType == 0x01)  //acrescentado na implementacao de MZ
				{
					// message cames on position Offset + Header Size
					byte[] buf_1 = new byte[buf.length-(HEADER_LENGHT + ofs)];
					for (int i=(HEADER_LENGHT + ofs) ; i<buf.length; i++)
						buf_1[i-(HEADER_LENGHT + ofs)] = buf[i]; 
					
					// HR: change to support EPMS encoding for special chars
					//response_msg = Util.ByteArrayToString(buf_1);			
					response_msg = new String(buf_1,"ISO-8859-1");
					System.out.println("Receiving message: " + response_msg); // ... Offset info : " + ofs);
					
				}
				else
					response_msg="";
				
//				if (_tpMsg == 0x01 || _tpMsg == 0x02) { // mensagem recuperavel (0x01) .. o 0x02 foi acrescentado na implementacao de MZ
//					bb.setLength(HEADER_LENGHT);
//					sio.write(bb);
//				}				

				System.out.println("TP MMT ref: " + request_mmt_reference + " *** msg: " + _tpMsg + " *** Code Resp: " + response_code );
				
				response_code = ""+rspType; // the code must be assigned only in the end because is used to control the response on caller. If seted before can cause not run the remaing code
			}

			//sio.close(); // usado na pratica para fechar a connection visto q cada ligacap so faz 1 pedido
			
			
		} catch (Exception e) {
			response_code = "2000";
			response_msg = "Unexpected error while processing dspMessage on PRT layer.";
			System.out.println("Error while receiving dspMessage (" + request_mmt_reference + ") from PRT" + e);
			e.printStackTrace();
		}
		finally
		{
			//-> nao se pode colocar isto senao duplica o pedido no EPMS
			//if (sio != null) // acrescentado na implementacao de MZ
			//	sio.close();			
		}
	}	
	
	public void showConnectionStatus(PersConnection conn, SocketIO _sio,
			int pevt) 
	{	
			try 
			{
				
				if (PCON_CONNECT == pevt) {
					//Logger.Debug("PRT connected. mxPed:" + _mxPed + "; nSeq:" + _nseq + "; Sequence:" + sequenceNo);
					//Logger.Debug("PRT connected (" + request_mmt_reference + "). mxPed:" + _mxPed + "; Sequence:" + sequenceNo);
					System.out.println("PRT connected (" + request_mmt_reference + "). Sequence:" + sequenceNo);
					
					/*
					if (_mxPed > 0) {
						_nseq = 0;
						formPedido(_sio);
					}
					*/
					Thread.sleep(1000);
					formPedido(_sio);

					
				}
				if (PCON_DISCNCT == pevt) {
					System.out.println("PRT disconnected (" + request_mmt_reference + ").");
					
					
					
				}
			} catch (Exception e) {
				response_code = "3000";
				response_msg = "Unexpected error while processing showConnectionStatus on PRT layer.";

				System.out.println("Error while handling showConnectionStatus from PRT." + e);
			}

	}

	public void formPedido(SocketIO sio) //throws Exception {
	{
		try
		{

			int i = sequenceNo;
			/*
			int i = ++_nseq;
			if (i > _mxPed) {
				if (_mxPed > 0) {
					sio.close();
				}
				return;
			}
			*/

//			StringBuffer sbf = new StringBuffer();
//			sbf.append((char) 0x01); // D.original
//			sbf.append((char) (i / 256)); // Token 1
//			sbf.append((char) (i % 256)); // Token 2
//			sbf.append((char) _tpMsg); // Tipo Msg
//			sbf.append((char) (_tmOut / 256)); // Token 1
//			sbf.append((char) (_tmOut % 256)); // Token 1
//			sbf.append(_epmsMsg);
			//sbf.append(i); // Token 1
//			String _epmsMsgWithHeader = "123456789012" + _epmsMsg;
//			sio.write(_epmsMsgWithHeader);
			
//			Thread.sleep(1000);
			
//			String _epmsMsgWithHeader2 = "123456789012" + _epmsMsg;
//			sio.write(_epmsMsgWithHeader2);
			
//			Logger.Debug("Message (" + request_mmt_reference + ") writed on out buf");
			
		}
		catch (Exception ex)
		{
			System.out.println("Error while writing on PRT channel" + ex);
			response_code = "4000";
			response_msg = "Unexpected error while processing formPedido on PRT layer.";
		}
	}
	
	

	public static void main(String args[]) {
		PrtCliReceptor prtrec = new PrtCliReceptor();
		prtrec.setEpmsMsg("E501001602000025640 202102051221420816020002560001878346139000000187111111000000001655497300000140001FILDA                    nome longo lala1Z8O3PE7N5TH2UGX");
		try {
			PersServerConnection clt = new PersServerConnection(9045);
			clt.setLogStream(new LogStream());
			clt.setDebug(3);
			System.out.println("A Fazer o Call!");
			clt.connect(prtrec);
			while (true) {
				System.out.println("Running!");
				Thread.sleep(10000);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
	}
}