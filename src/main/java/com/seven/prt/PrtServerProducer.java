package com.seven.prt;

import sibs.deswin.lib.com.IPersComConsumer;
import sibs.deswin.lib.com.PersConnection;
import sibs.deswin.lib.com.SocketIO;
import sibs.deswin.lib.io.ByteBuffer;

public class PrtServerProducer implements IPersComConsumer {

    private final int messageType = 0x01; // 0x01 - request message
    private final int OUTBOUND_TIMEOUT = 3000;  //timeoutOutbound = Config.getValue(cfgServiceVars.get("TimeoutOutbound"), 3000);
    private final int messageTimeout = OUTBOUND_TIMEOUT/1000;  // In seconds, only used when message type its not recoverable (0x00)
    private final int recoverType = 0x00; // 0x00 - not recoverable, 0x01 - recoverable

    private SocketIO socketIO;

    public void send(String requestMessage) {
        if (socketIO == null) {
            throw new IllegalStateException("PRT outbound session not connected");
        }

        int correlationId = 1;
        String message = String.valueOf((char) messageType) + // D.original
                (char) (correlationId / 256) + // Sequence number token 1
                (char) (correlationId % 256) + // Sequence number token 2
                (char) recoverType + // recover type
                (char) (messageTimeout / 256) + // Message timeout token 1
                (char) (messageTimeout % 256) + // Message timeout token 1
                requestMessage;

        try {
            socketIO.write(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showConnectionStatus(PersConnection persConnection, SocketIO socketIO, int connectionStatus) {
        switch (connectionStatus) {
            case PCON_CONNECT:
                System.out.println("Connected");
                this.socketIO = socketIO;
                String header = "123456";
                String message = "E501001602000000012 202102051221420816020002560001878346139000000187111111000000001655497300000140001FILDA                    nome longo lala1Z8O3PE7N5TH2UGX";
                this.send(header + message);
                break;
            case PCON_DISCNCT:
            case PCON_TIMEOUT:
                this.socketIO = null;
                break;
            default:
                throw new IllegalStateException("Invalid PRT session connection status");
        }
    }

    @Override
    public void dspMessage(SocketIO socketIO, ByteBuffer byteBuffer) {
        int offset = byteBuffer.getOffset();
    }

}
