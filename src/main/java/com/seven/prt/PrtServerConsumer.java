package com.seven.prt;

import sibs.deswin.lib.com.IPersComConsumer;
import sibs.deswin.lib.com.PersConnection;
import sibs.deswin.lib.com.SocketIO;
import sibs.deswin.lib.io.ByteBuffer;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class PrtServerConsumer implements IPersComConsumer {

    @Override
    public void showConnectionStatus(PersConnection persConnection, SocketIO socketIO, int connectionStatus) {
        switch (connectionStatus) {
            case PCON_CONNECT:
                System.out.println("PRT outbound session connected!");
                break;
            case PCON_DISCNCT:
            case PCON_TIMEOUT:
                System.out.println("PRT outbound session disconnected!");
                break;
            default:
                throw new IllegalStateException("Invalid PRT session connection status");
        }
    }

    @Override
    public void dspMessage(SocketIO socketIO, ByteBuffer byteBuffer) {
        int offset = byteBuffer.getOffset();
        byte[] bufferRequest = byteBuffer.getBuffer();

        byte[] commandArray = Arrays.copyOfRange(bufferRequest, offset + 6, offset + 10);
        String commandStr = new String(commandArray, StandardCharsets.UTF_8);

        String message = getResponseMessage(commandStr);

        byte[] messageByteArray = message.getBytes(StandardCharsets.UTF_8);
        int messageByteArraySize = messageByteArray.length;
        int messageFullSize = messageByteArraySize + 16;

        byte[] sequenceNumArray = Arrays.copyOfRange(bufferRequest, offset + 1, offset + 3);
        byte[] bufferResponse = new byte[messageFullSize];
        bufferResponse[0] = 0x00;
        bufferResponse[1] = sequenceNumArray[0];
        bufferResponse[2] = sequenceNumArray[1];
        bufferResponse[3] = 0x00;

        System.arraycopy(bufferRequest, 2, bufferResponse, 4, 12);
        System.arraycopy(messageByteArray, 0, bufferResponse, 16, messageByteArraySize);

        try {
            socketIO.write(bufferResponse);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getResponseMessage(String command) {
        String message = "";
        switch (command) {
            case "E001": // Payment confirm
                message = "R00101900000500000010313887H202002241635340000815850000646504ca1f0000000031388799";
                break;
            case "E003": // Special Service Confirm
                message = "R00303900003400000010313749H202002241501310000815850000619004c9950000000031374981TEXTO NORMAL UUUUUUUUUUUUUUUUUUUUUUUUUUU\\n     ABCDE23569AAB00009            \\n                    \\n                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ";
                break;
            case "E005": // Special Service - Retrieve List
                message = "R00501000003000000010314215H2020022615310400008158500008367810401Teo Pacote 1      Pacote Desporto   Nº Cartão Teo  11Ref 1                         Ref 2                         0102Teo Pacote 3      Teo Novelas       Nº Cartão Teo  11Ref1                          Ref2                          0203Teo Pacote 2      Teo Cinema        Nº Cartão Teo  12Ref 1                         Ref 2                         0304Teo Pacote 4      Pag Simples Teo   Referência     11*                             *                             04";
                break;
            case "E006": // Transfer Confirm
                message = "R00600900000500000010313699H202002241230470000815850000603204c963000000003136991BRN; \"M&NU[L\" ??RRIR/NEVESALEXANDRE     ";
                break;
            case "E010": // Balance Query
                message = "R01000900000500000010310377H2020021015361700008157800140435100003329231001602407777777777C09999999999C20200210";
                break;
            case "E011": // Movement Query
                message = "R01100900000500000010310380H202002101536530000815780014043902407777777777C09999999999C2020021008LEVLevantamento   2008040100000035000D024CADAdiantamento   2008040200000015760C024LEVLevantamento   2008040100000035000D024CADAdiantamento   2008040200000015760C024LEVLevantamento   2008040100000035000D024CADAdiantamento   2008040200000015760C024LEVLevantamento   2008040100000035000D024CADAdiantamento   2008040200000015760C024";
                break;
            case "E012": // Account Activation
                message = "R01200000003000000010298862H202001131634070000815780008096400000000017244900000500000018711111101Vodafone                                00050000100000100050001050000100013216050028******2118   2512001V";
                break;
            case "E013": // Login
                message = "R01300000003000000010314320H202002261757330000815850000860100000000023619202002261737480300340000000003100340001340000000017316483381******0262   2212001 9000034V00050000100000100050001050000100013216050028******2118   2512002 9000005V00050000100000100050001050000100013216050028******2111   2512003 9000005V000000000000000000000000000000000000000000";
                break;
            case "E015": // Transfer Request
                message = "R01500900000500000010313133H2020022017330100008158500002806BRN; \"M&NU[L\" ??RRIR/NEVESALEXANDRE     ";
                break;
            case "E017": // Card Activate
                message = "R01700900000500000010314037H2020022610583800008158500007983002";
                break;
            case "E018": // Change PIN
                message = "R01800000003000000010314122H2020022614172900008158500008190";
                break;
            case "E021": // Card Remove
                message = "R02100900000500000010313829H2020022415462900008158500006350";
                break;
            case "E022": // Activate
                message = "R02200000003000000010447412H20210223151644000081602000436050799935800000000";
                break;
            case "E023": // Cash Withdraw Request
                message = "R02300900000500000010312015H202002191310270000815850000096604c2cf00000000312015594D1714C9C109B5C722051A13BD3CA149CE2454E3D8E60BB3D7C782E838E60120200219141027";
                break;
            case "E025": // Cash Withdraw Query
                message = "R02500900000500000010310393H2020021015384600008157800140455200000001000000973A202001281231282020012813312800000006090000000000000000000001000000973A202001281210112020012813101100000006080000000000000000000001500000973A202001270921052020012710210500000006050000000000000000000001500000973C202001201710032020012018100300000006030000000000000000000000300000973C201912181231192019121813311900000005760000000000000000000000300000973A201912161347362019121614473600000005720000000000000000000001500000973A201912031504232019120316042300000005670000000000000000000000300000973A201910021606532019100217065300000003640000000000000000000000300000973A201909201637262019092017372600000003580000000000000000000000300000973A201909201611272019092017112700000003570000000000000000000000300000973C201909191607292019091917072900000003560000000000000000000000300000973C201909111708242019091118082400000003300000000000000000000001800000973A201909111541012019091116410100000003270000000000000000000001900000973A201909111528242019091116282400000003260000000000000000000000300000973A201909111519542019091116195400000003250000000000000000000000300000973C201909101753172019091018531700000003060000000000000000000000300000973A201909060847352019090609473500000002870000000000000000000000200000973A201909031616112019090317161100000002700000000000000000000000400000973A201909031615542019090317155400000002690000000000000000000000200000973A20190903155154201909031651540000000268000000000000000";
                break;
            case "E026": // Cash Withdrawal Cancel
                message = "R02600900000500000010298068H2020010716132000008157800073769";
                break;
            case "E027": // Cash Withdraw Get Detail
                message = "R02700900000500000010307476H20200129134524000081578001091180000000300000973A20200129134511202001291445110000000000000005211515CD1C0143E6CC132900C776D99924464AA441F11ABCC2D8595D4335F8B";
                break;
            case "E028": // P2P Transfer
                message = "R02800900003400000010314078H20200226120046000081585000080590000000031407820202002261205465";
                break;
            case "E029": // P2P Transfer Accept No PIN
                message = "R02900900000500000010314081H2020022612010100008158500008061000000003140785";
                break;
            case "E031": // Get Cards Bin
                message = "R03100000003000000010314321H202002261757330000815850000860203001483381000020500289000305002890";
                break;
            case "E033": // Card Lock
                message = "R03301900000500000010312015H2020021913102700008158500000966V";
                break;
            case "E035": // Card Update/Card Delete
                message = "R03500900000500000010312015H2020021913102700008158500000966";
                break;
            case "E036": // Card Get Limit
                message = "R03600900000500000010312015H202002191310270000815850000096600000000300001973";
                break;
            default:
                return "";
        }

        return message;
    }
}
