package com.seven.prt;

import sibs.deswin.lib.com.PersServerConnection;
import sibs.deswin.lib.util.LogStream;

public class PrtApplication {

    private final static int DEBUG_LEVEL = 3;
//    private final static int SERVER_PORT = 9045;
    private final static int SERVER_PORT = 9047;

    public static void main(String[] args) throws Exception {
//        PrtServerProducer server = new PrtServerProducer();
        PrtServerConsumer server = new PrtServerConsumer();
        System.out.println("Initializing PRT");

        PersServerConnection serverConn = new PersServerConnection(SERVER_PORT);
        serverConn.setLogStream(new LogStream());
        serverConn.setDebug(DEBUG_LEVEL);
        serverConn.connect(server);
        while (true) {
            System.out.println("Running...");
            Thread.sleep(10000);
        }
    }

}